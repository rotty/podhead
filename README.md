# PodHead

A stand-alone Python tool, which can add chapter marks to Vorbis
(OGG) files (MP3 is on the TODO list). In addition, it intends to help
with the following podcast-publishing workflows:

(1) Audacity -> Wordpress/PodLove

This is a simple job, but doing it by hand every time sucks, one
would imagine:

Audacity exports the chapter marks in a format not /quite/ suited
for pasting into PodLove. This tool takes the .txt chapter mark file
produced by Audacity, and essentially cuts away the second
column. It also takes care of the Audicty stupidity of using locale
settings when writing floats to the chapter mark files, at least for
the case where the locale uses a comma instead of a decimal point,

(2) Audacity -> Jekyll

This is a bit more involved. Here the chapter marks are to be embedded
into the feed, and in addition should show up in the HTML page, maybe
using some newfangled HTML5 tags. The latter two tasks are out of the
scope for podhead, as they can be done withing Jekyll, for example
using octopod. Chapter marks are embedded in the page front matter
YAML. Octopod should however create the front matter, and combine it
with content prepared elsewhere, e.g. in a an etherpad.

## Status

Adding chapter marks to vorbis files is implemented, as well as
conversion to a format to paste into the PodLove Wordpress plugin.

TODO items still left:

- Etherpad markup grabbing
- Embedding chapter marks into the RSS feed
- Embedding chapter marks into HTML5
